const {clientHandler} = require('./clients')
const {streamHandler} = require('./streamers')
const {turnServer} = require('./turn')
const {researchAssistants} = require("./researchassistants")

const {logger} = require("./logger")

require('dotenv').config()

//process.env.DEBUG = '*'
const io = require('socket.io')(3000, {
    cors: {
        origin: true
    }
})
turnServer.init()
researchAssistants.init()

setTimeout(()=>turnServer.addUser('test','me'),1000)

const experimentCreds = {'Feedback Study': '123abc!'}

console.log("I'm here and listening")
io.on('connection', socket => {
    console.log('Connection established')

    //Collect timing informations for every socket
    //https://developers.google.com/web/updates/2012/08/When-milliseconds-are-not-enough-performance-now
    let starttime
    socket.timeOffset = []
    socket.roundTrip = []
    const timeSamples = process.env.LATENCY_SAMPLES
    socket.on('pong',(systime)=>{
        let endtime = Date.now()
        if(socket.roundTrip.length >= timeSamples) socket.roundTrip.shift()
        socket.roundTrip.push(endtime-starttime)
        if(systime){
            if(socket.timeOffset.length >= timeSamples) socket.timeOffset.shift()
            socket.timeOffset.push(systime-(endtime-(endtime-starttime)/2))
            //console.log("offset",socket.timeOffset)
            //console.log("roundtrip",socket.roundTrip)
        }
    })
    setInterval(()=>{
        starttime = Date.now()
        socket.emit('ping')
    },process.env.LATENCY_SAMPLE_RATE)

    socket.on('registerClient',(clientInfo,cb)=>{
        console.log("Client connected",clientInfo)
        if(clientInfo.clientType==='MTLGClient') clientHandler.addClient(socket,clientInfo);
        streamHandler.createChannel(clientInfo.clientID + clientInfo.roomID)
        cb(clientInfo.clientID + clientInfo.roomID)
    })

    socket.on('authenticateResearchAssistant',(credentials,cb)=>{
        res=researchAssistants.authenticate(credentials.user,credentials.password)
        if(res){
            cb("Success")
            socket.username = res.username
            socket.level = res.level
            socket.on("getClients",(room,cb)=>{
                console.log("getClientsreceived",room)
                console.log("getClientsinRoomresult",clientHandler.getClientsInRoom(room))
                cb(clientHandler.getClientsInRoom(room))
            })
            socket.on("requestSources",async (cid,type,cb)=>{
                console.log("requestSources received",cid,type)
                cb(await clientHandler.getClientSources(cid,type))
            })
            socket.on("proceed",(cid)=>{
                clientHandler.proceed(cid)
            })
            socket.on("sendSequenceToClient",(cid, sequence)=>{
                //THIS IS NOT HOW THIS IS INTENDED TODO
                //the idea is to send an experiment to the orchestrator with defined sequences for clients in rooms. the orchestrator will distribute them
                clientHandler.sendSequence(cid,sequence)
            })
            socket.on("requestStream",(cid,streamID,streamSourceType)=>{
                clientHandler.requestStream(cid,streamID,streamSourceType)
            })
        }
        else cb("Authentication failed")
    })

    //todo will have to refactor. This is only valid for client type 5, remote experiment without research assistant
    socket.on("joinExperiment",(experimentInfo)=>{
        console.log("Trying to join Exp",experimentInfo)
        console.log("For reference:",experimentCreds)
        console.log("experimentInfo.experimentAuthToken",experimentInfo.experimentAuthToken)
        console.log("experimentCreds[experimentInfo.experimentID]",experimentCreds[experimentInfo.experimentID])
        if(experimentInfo.experimentAuthToken === experimentCreds[experimentInfo.experimentID]){

            socket.join(experimentInfo.experimentId);
            console.log("Experiment successfully joined");
        }
    })
    socket.on("joinBroadcastChannel", (r)=>{

        streamHandler.joinChannel(r,socket)
        socket.on("broadcaster", () => {
            streamHandler.addBroadcaster(socket,r)
        });
        socket.on("watcher", () => {
            streamHandler.addWatcher(socket)
        });
    })

  /*  socket.on('disconnect',(reason)=>{
        console.log("A client disconnected")
        mtlgClients.delete(socket)
    })*/



})
