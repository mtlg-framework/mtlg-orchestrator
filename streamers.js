let broadcaster;
let channels = {}

let watching = {}

function joinChannel(channel, socket){
    watching[socket.id] = channel
    socket.join(channel)
}

function addBroadcaster(socket,channel){
    //broadcaster = socket.id;
    channels[channel] = socket.id
    socket.to(channel).emit("broadcaster");
    console.log("A broadcaster has been announced")
    handleConnection(socket,channel)
}

function addWatcher(socket){
    const channel = watching[socket.id]
    /* channels.forEach((c,i)=>{
        if(i!==channel) socket.to(c.emit("disconnectPeer", socket.id))
    })*/
    console.log("all channels",channels)
    console.log("Watcher interested in",channel)
    for(let i in channels) {
        if(i!==channel) {
            socket.to(channels[i]).emit("disconnectPeer", socket.id)
            console.log("I asked ",i,"to disconnect ",socket.id)
        }

    }
    socket.to(channels[channel]).emit("watcher", socket.id);
    //console.log("A watcher has announced interest")
    handleConnection(socket,channel)
}

function createChannel(roomID){
    console.log("Intention to create room",roomID)
}

function handleConnection(socket,channel){
    socket.on("offer", (id, message) => {
        socket.to(id).emit("offer", socket.id, message);
        console.log("An offer has been made")
    });
    socket.on("answer", (id, message) => {
        socket.to(id).emit("answer", socket.id, message);
        console.log("An answer has been given");
    });
    socket.on("candidate", (id, message) => {
        socket.to(id).emit("candidate", socket.id, message);
        console.log("A candidate has been found")
    });
    socket.on("disconnect", () => {
        if(channels[channel]==socket.id) channels[channel]=undefined;
        socket.to(channels[channel]).emit("disconnectPeer", socket.id);
        console.log("Someone disconnected")
    });
}

exports.streamHandler = {
    addBroadcaster,
    addWatcher,
    createChannel,
    joinChannel
}
