const redis = require("redis")

const redisClient = redis.createClient({host:"redis"})

redisClient.set("pizza","Pasta")

function log(key, message, level){
    key = `log:experiment:${key}:test`
    redisClient.set(key,message,redis.print)//,'EX',60

    //todo persistence only for acknowledged users, set to sane values (like two weeks) for everyone else
    //this should be set in an env variable
    //todo derive some quality format for logging. like log:{experiment-id}:{clientID}:{module}:{DateTime}:{logLevel}
    //logLevel? Verbose, Debug, Info, Warn, Error, Fatal
}

log("Pizza","Paste",0)
exports.l  = {

}
