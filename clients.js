let io
const mtlgClients = []
const clientSockets = {}

function init(socketManager) {
    io = socketManager
    console.log("ClientHandler initialized")
}

function addClient(clientSocket, clientInfo) {
    clientInfo.socket = clientSocket.id
    mtlgClients.push(clientInfo)
    clientSockets[clientSocket.id] = clientSocket
    clientSocket.on("disconnect",()=>{
        const index = mtlgClients.indexOf(clientInfo);
        if (index > -1) {
            mtlgClients.splice(index, 1);
        }
        delete clientSockets[clientSocket.id]
    })
}

function getClientsInRoom(roomID) {
    console.log("current mtlgClients array might be reduced",mtlgClients)
    return mtlgClients.map((c) => {
        if(c.roomID === roomID) return c
    })
}

function getClientSources(clientSocket,type) {
    console.log("getClientSource called",clientSocket,type)
    return new Promise((resolve,reject)=>{
        setTimeout(reject,10000)
        clientSockets[clientSocket].emit("getAvailableSources",type, (sources)=>{
            console.log("list of received resources",sources)
            resolve(sources)
        })
    })
}

function requestStream(clientSocket,sourceID,sourceType){
    clientSockets[clientSocket].emit("requestStream",sourceID,sourceType)
}

function proceed(clientSocket){
    clientSockets[clientSocket].emit("nextStage");
}

function sendSequence(clientSocket,sequence){
    clientSockets[clientSocket].emit("loadSequence",sequence);
}

exports.clientHandler = {
    init,
    addClient,
    getClientsInRoom,
    getClientSources,
    sendSequence,
    requestStream,
    proceed
}
