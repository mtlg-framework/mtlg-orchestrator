const fs = require('fs')
const crypto = require('crypto')
require('dotenv').config()

// const users = JSON.parse((fs.readFileSync("./users.json")))

// user levels might be necessary in future application, setting 5 for superuser should suffice for now
// considerations on that:
// 1 might be needed to use at all
// 2 might be required for rtc-signaling
// 3 might be required for sync use of local logger and TURN-server
// 4 might be required for persisting experiments and packages and permanent room claiming
// 5 might be required for managing user rights

let users = []

function init() {
    if(fs.existsSync("./users.json")) {
        users = JSON.parse((fs.readFileSync("./users.json")))
        console.log("Loaded users from fs")
    }
    if (users.length === 0) {
        console.log("No users present yet, generating from environment")
        addUser(process.env.ADMIN, process.env.PASSWORD, 5);
    }
}

function verifyPassword(password, hash, salt) {
    const hashVerify = crypto.pbkdf2Sync(password, salt, 10000, 64, "sha512").toString("hex");
    return hash === hashVerify;
}

function generatePassword(password) {
    const salt = crypto.randomBytes(32).toString("hex");
    const hash = crypto.pbkdf2Sync(password, salt, 10000, 64, "sha512").toString("hex");
    return {
        salt: salt,
        hash: hash,
    };
}

function addUser(username, password, level) {
    const {salt, hash} = generatePassword(password)
    let user = {
        username,
        salt,
        hash,
        level
    }
    users.push(user)
    saveUsers()
}

function saveUsers() {
    //might be moved to DB connection in the future
    fs.writeFileSync("./users.json", JSON.stringify(users))
    console.log("saved users to filesystem")
    //const users = JSON.parse((fs.readFileSync("./users.json")))
}

function authenticate(uname, pw){
    const candidate=users.find((u)=>u.username===uname)
    if(verifyPassword(pw,candidate.hash,candidate.salt)) return candidate
    else return false;
}

exports.researchAssistants = {
    init,
    authenticate
}
