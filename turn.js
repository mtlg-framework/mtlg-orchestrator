const Turn = require('node-turn');
const publicIp = require('public-ip');

let server;

async function init(){
    let options = {
        // set options
        authMech: 'short-term',
        credentials: {
            username: "password"
        },
        debugLevel: 'ALL',
        externalIps:await publicIp.v4()
    }
    server = new Turn(options);

    server.start()

    console.log(server.listeningIps)
    console.log(server.listeningPort)
    console.log(server.externalIps)
}
function addUser(name,password){
    server.addUser(name,password)
}
function removeUser(name){
    server.removeUser(name)
}
function stopServer(){
    server.stop()
}

exports.turnServer = {
    init,
    addUser,
    removeUser,
    stopServer
}
